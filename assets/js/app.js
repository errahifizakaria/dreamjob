require('../css/app.scss');
require('../js/canvas.js');
require('materialize-css');
const tippy = require('tippy.js');
var $ = require('jquery');

$(document).ready(function() {
    $('input[type="checkbox"]').on('change', function() {
        $(this).siblings('input[type="checkbox"]').not(this).prop('checked', false);
    });

});
