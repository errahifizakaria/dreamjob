<?php

/* default/questiontwo.html.twig */
class __TwigTemplate_91a9eae9d355a34ded3b9e6cbc9a278dde6ba83adfaaf5c228dea5f6778228d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/questiontwo.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
    <div id=\"questionOne\" class=\"back_questions\">
        <div class=\"logo\"><img class=\"logo_img\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\" alt=\"\"></div>

        <div class=\"container_tablet_question\">
            ";
        // line 10
        echo "            <h1 class=\"title_q1\">étape 02: retour en enfance </h1>
            <div class=\"content-q1\">
                <p>Avec ton Stylus, reprends à l'écrit la phrase suivante : BIC vous en donne toujours plus !</p>
                <div id=\"sketch\">
                    <canvas id=\"canvas\" width=\"530\" height=\"170\"></canvas>
                </div>
                <a href=\"";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("questionthree");
        echo "\" class=\"btn_submit3 save\">\" Valider \"</a>
                <div class=\"errors\">

                </div>
            </div>
        </div>


    </div>

";
    }

    // line 28
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 29
        echo "
";
    }

    public function getTemplateName()
    {
        return "default/questiontwo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 29,  65 => 28,  50 => 16,  42 => 10,  36 => 6,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/questiontwo.html.twig", "/home/dreamjob/public_html/app/Resources/views/default/questiontwo.html.twig");
    }
}
