<?php

/* default/inscription.html.twig */
class __TwigTemplate_c7cadd2ff6f05c550b34bb7ea6822e4f9ce774cf8658b06ec0f9bc3797db615f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/inscription.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
    <div id=\"inscription\">
        <div class=\"logo\"><img class=\"logo_img\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\" alt=\"\"></div>

        <div class=\"container_tablet\">
            ";
        // line 10
        echo "            <h1 class=\"title_intro2\">\" Inscris - toi \"</h1>
            ";
        // line 11
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', array("attr" => array("novalidate" => "novalidate", "class" => "form_inscription")));
        echo "


            <div class=\"form_control\">
                ";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "nom", array()), 'widget', array("attr" => array("placeholder" => "Nom")));
        echo "

                <span class=\"error_msg\">";
        // line 17
        echo twig_escape_filter($this->env, strip_tags($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "nom", array()), 'errors')), "html", null, true);
        echo "</span>
            </div>
            <div class=\"form_control\">
                ";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "prenom", array()), 'widget', array("attr" => array("placeholder" => "Prenom")));
        echo "
                <span class=\"error_msg\">";
        // line 21
        echo twig_escape_filter($this->env, strip_tags($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "prenom", array()), 'errors')), "html", null, true);
        echo "</span>
            </div>
            <div class=\"form_control\">
                ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "email", array()), 'widget', array("attr" => array("placeholder" => "Email")));
        echo "
                <span class=\"error_msg\">";
        // line 25
        echo twig_escape_filter($this->env, strip_tags($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "email", array()), 'errors')), "html", null, true);
        echo "</span>
            </div>
            ";
        // line 28
        echo "            ";
        // line 29
        echo "            ";
        // line 30
        echo "            ";
        // line 31
        echo "            <div class=\"form_control control_inscription\">
            <ul class=\"list_matiers list_inscription\">
                ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["form"] ?? null), "gender", array()), "children", array()));
        foreach ($context['_seq'] as $context["key"] => $context["matiersItem"]) {
            // line 34
            echo "                    <li>
                        <label for=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["matiersItem"], "vars", array()), "id", array()), "html", null, true);
            echo "\"
                               class=\"control control--checkbox\">";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["matiersItem"], "vars", array()), "label", array()), "html", null, true);
            echo "
                            ";
            // line 37
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["matiersItem"], 'widget');
            echo "
                            <div class=\"control__indicator\"></div>

                        </label>
                    </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['matiersItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "            </ul>
                <span class=\"error_msg\">";
        // line 44
        echo twig_escape_filter($this->env, strip_tags($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? null), "gender", array()), 'errors')), "html", null, true);
        echo "</span>
            </div>
            <button type=\"submit\" class=\"btn_submit2\">\" s'inscrire \"</button>

            ";
        // line 48
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
            <div class=\"errors center-align\">


            </div>
        </div>


    </div>

";
    }

    // line 60
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 61
        echo "
";
    }

    public function getTemplateName()
    {
        return "default/inscription.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 61,  144 => 60,  129 => 48,  122 => 44,  119 => 43,  107 => 37,  103 => 36,  99 => 35,  96 => 34,  92 => 33,  88 => 31,  86 => 30,  84 => 29,  82 => 28,  77 => 25,  73 => 24,  67 => 21,  63 => 20,  57 => 17,  52 => 15,  45 => 11,  42 => 10,  36 => 6,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/inscription.html.twig", "/home/dreamjob/public_html/app/Resources/views/default/inscription.html.twig");
    }
}
