<?php

/* default/result.html.twig */
class __TwigTemplate_a7f0989f50e4fb9b79d40c7d2a8b1aedde5ffcf9c8e67623e9d2296bc30ca2ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/result.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
    <div id=\"questionOne\" class=\"back_questions\">
        <div class=\"logo\"><img class=\"logo_img\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\" alt=\"\"></div>

        <div class=\"container_tablet_question\">
            ";
        // line 10
        echo "            <h1 class=\"title_q1\">Vous êtes fait pour être :</h1>
            <div class=\"content-q1 flex_\">
                ";
        // line 13
        echo "               <h2 class=\"title_result\" >";
        echo twig_escape_filter($this->env, ($context["metier"] ?? null), "html", null, true);
        echo "</h2>
               <div class=\"container_img\" style=\"background-image: url(";
        // line 14
        echo twig_escape_filter($this->env, ($context["image"] ?? null), "html", null, true);
        echo ")\">
                    <!-- <img src=\"";
        // line 15
        echo twig_escape_filter($this->env, ($context["image"] ?? null), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, ($context["metier"] ?? null), "html", null, true);
        echo "\"> -->

                <a href=\"";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\" class=\"btn_submit5\">\" TERMINER \"</a> 
               </div>
              

            </div>
        </div>


    </div>

";
    }

    // line 29
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 30
        echo "
";
    }

    public function getTemplateName()
    {
        return "default/result.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 30,  77 => 29,  62 => 17,  55 => 15,  51 => 14,  46 => 13,  42 => 10,  36 => 6,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/result.html.twig", "/home/dreamjob/public_html/app/Resources/views/default/result.html.twig");
    }
}
