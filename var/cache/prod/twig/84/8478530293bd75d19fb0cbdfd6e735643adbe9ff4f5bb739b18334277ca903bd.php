<?php

/* default/questionone.html.twig */
class __TwigTemplate_d9bf32deec53fcdac28a18b89ef5dae25bd45bc0c1104a9e55d5e42dec1a319c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/questionone.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
    <div id=\"questionOne\" class=\"back_questions q1 \">
        <div class=\"logo\"><img class=\"logo_img\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\" alt=\"\"></div>

        <div class=\"container_tablet_question\">
            ";
        // line 10
        echo "            <h1 class=\"title_q1\">étape 01: Un autographe s'il te plait </h1>
            <div class=\"content-q1\">
                <p>Pour commencer, merci de griffonner, à la manière des stars, un autographe pour nous !</p>
                <div id=\"sketch\">
                    <canvas id=\"canvas\" width=\"530\" height=\"170\"></canvas>

                </div>
                <a href=\"";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("questiontwo");
        echo "\" class=\"btn_submit3 save\" id=\"save\"  >\" Valider \"</a>
                <div class=\"errors\">

                </div>
            </div>



        </div>


    </div>

";
    }

    // line 32
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 33
        echo "
";
    }

    public function getTemplateName()
    {
        return "default/questionone.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 33,  69 => 32,  51 => 17,  42 => 10,  36 => 6,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/questionone.html.twig", "/home/dreamjob/public_html/app/Resources/views/default/questionone.html.twig");
    }
}
