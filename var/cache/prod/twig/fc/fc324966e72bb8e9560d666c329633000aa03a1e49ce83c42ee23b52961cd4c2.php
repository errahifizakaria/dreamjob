<?php

/* default/questionthree.html.twig */
class __TwigTemplate_7979cec97c34793ea7063a29288e13c40a34464c529332c5b9d0c2b5db5ee210 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/questionthree.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
    <div id=\"questionOne\" class=\"back_questions\">
        <div class=\"logo\"><img class=\"logo_img\" src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\" alt=\"\"></div>

        <div class=\"container_tablet_question\">
            ";
        // line 10
        echo "            <h1 class=\"title_q1\">étape 03: à l'école, tu préférais:</h1>
            <div class=\"content-q1\">
                ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', array("attr" => array("class" => "form_question3")));
        echo "



                <ul class=\"list_matiers \">
                    ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["form"] ?? null), "nom", array()), "children", array()));
        foreach ($context['_seq'] as $context["key"] => $context["matiersItem"]) {
            // line 18
            echo "                        <li>
                            <label for=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["matiersItem"], "vars", array()), "id", array()), "html", null, true);
            echo "\"
                                   class=\"control control--checkbox\">";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["matiersItem"], "vars", array()), "label", array()), "html", null, true);
            echo "
                                ";
            // line 21
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["matiersItem"], 'widget');
            echo "
                                <div class=\"control__indicator\"></div>

                        </label>
                        </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['matiersItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "                </ul>
                ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "
                <button type=\"submit\" class=\"btn_submit4\">\" valider \"</button>
                ";
        // line 31
        echo "                ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
            </div>
        </div>


    </div>

";
    }

    // line 40
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 41
        echo "
";
    }

    public function getTemplateName()
    {
        return "default/questionthree.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 41,  102 => 40,  89 => 31,  84 => 28,  81 => 27,  69 => 21,  65 => 20,  61 => 19,  58 => 18,  54 => 17,  46 => 12,  42 => 10,  36 => 6,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/questionthree.html.twig", "/home/dreamjob/public_html/app/Resources/views/default/questionthree.html.twig");
    }
}
