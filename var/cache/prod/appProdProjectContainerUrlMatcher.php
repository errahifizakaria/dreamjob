<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        // homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'homepage'));
            }

            return $ret;
        }
        not_homepage:

        // inscription
        if ('/inscription' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::inscriptionAction',  '_route' => 'inscription',);
        }

        if (0 === strpos($pathinfo, '/q')) {
            // questionone
            if ('/q1' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::questionOneAction',  '_route' => 'questionone',);
            }

            // questiontwo
            if ('/q2' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::questionTwoAction',  '_route' => 'questiontwo',);
            }

            if (0 === strpos($pathinfo, '/q3')) {
                // questionthree33
                if ('/q33' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::questionThreeAction',  '_route' => 'questionthree33',);
                }

                // questionthree
                if ('/q3' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::createMatierAction',  '_route' => 'questionthree',);
                }

            }

        }

        // result
        if ('/result' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::resultAction',  '_route' => 'result',);
        }

        // add_category
        if ('/addcategory' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::createCategoryAction',  '_route' => 'add_category',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
