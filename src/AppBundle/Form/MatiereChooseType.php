<?php

namespace AppBundle\Form;

use AppBundle\Entity\Matiere;
use AppBundle\Repository\MatiereRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Category;
use Doctrine\ORM\EntityRepository;

class MatiereChooseType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom', EntityType::class, array(
            'mapped' => true,
            'class' => Matiere::class,
            'expanded' => true,
            'multiple' => true,

            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('u')
                    ->select('u')
                    -> from(Matiere::class, 'm')
                    ->orderBy('m.id', 'ASC')
//                    ->groupBy('u.nom')
                    ->distinct();
            },

            'choice_label' => 'nom',
        )

    );



    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Matiere'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_matiere';
    }


}
