<?php

namespace AppBundle\Form;

use AppBundle\Entity\Participants;
use AppBundle\Form\DataTransformer\GenderTransformer;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParticipantsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public $mydata;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('email', TextType::class)
            ->add('gender', ChoiceType::class,
                array(
                    'mapped' => true,
                    'expanded' => true,
                    'multiple' => true,
                    'label' => 'Employment',
                    'choices' => array(
                        'Homme' => 'homme',
                        'Femme' => 'femme'))
            );
        $builder->get('gender')->addModelTransformer( new GenderTransformer(), true);


        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {


        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Participants'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_participants';
    }


}
