<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Matiere;
use AppBundle\Entity\Participants;
use AppBundle\Form\CategoryType;
use AppBundle\Form\MatiereType;
use AppBundle\Form\MatiereChooseType;
use AppBundle\Form\ParticipantsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints\NotBlank;

class DefaultController extends Controller
{
    public $metiers = array(
        'Styliste de sushis' => 'Styliste de sushis',
        'Dessinateur des points de dés' => 'Dessinateur des points de dés',
        'Professeur de Sieste' => 'Professeur de Sieste',
        'Gouteur d’eau' => 'Gouteur d’eau',
        'Agent de circulation aérienne' => 'Agent de circulation aérienne',
        'Pousseur de trains en panne' => 'Pousseur de trains en panne',
        'Grand mère professionnelle' => 'Grand mère professionnelle',
        'Collecteur de plantes sauvages' => 'Collecteur de plantes sauvages',
        'Attrapeur de taureaux de Corrida' => 'Attrapeur de taureaux de Corrida',
        'Testeur de toboggans' => 'Testeur de toboggans',
        'Voix de boîte vocale' => 'Voix de boîte vocale',
        'Klaxon humain' => 'Klaxon humain',
        'Collecteur de balles de tennis' => 'Collecteur de balles de tennis',
        'Rangeur de quilles de bowling' => 'Rangeur de quilles de bowling',
        'Dresseur de fourmis' => 'Dresseur de fourmis',
        'Compteur de pandas' => 'Compteur de pandas',
        'Copieur Colleur' => 'Copieur Colleur',
        'Likeur de commentaires' => 'Likeur de commentaires'

    );
    public $matieres = array(
        'Les Maths' => 'Les Maths',
        'Art Plastique' => 'Art Plastique',
        'Littérature' => 'Littérature',
        'L\'espagnol' => 'L\'espagnol',
        'SVT' => 'SVT',
        'Musique' => 'Musique',
        'SPORT' => 'SPORT',
        'Physique Chimie' => 'Physique Chimie',
        'Comptabilité' => 'Comptabilité',

    );

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/inscription", name="inscription")
     */
    public function inscriptionAction(Request $request)
    {
        $participant = new Participants();
        $form = $this->createForm(ParticipantsType::class, $participant);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
           $em = $this->getDoctrine()->getManager();
           $em->persist($data);
          $em->flush();

   return   $this->redirectToRoute("questionone");


        }

        return $this->render('default/inscription.html.twig', array(
            'form' => $form->createView()

        ));
    }

    /**
     * @Route("/q1", name="questionone")
     */
    public function questionOneAction(Request $request)
    {

        return $this->render('default/questionone.html.twig');
    }

    /**
     * @Route("/q2", name="questiontwo")
     */
    public function questionTwoAction(Request $request)
    {

        return $this->render('default/questiontwo.html.twig');
    }

    /**
     * @Route("/q33", name="questionthree33")
     */
    public function questionThreeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository_matiere = $em->getRepository(Matiere::class);


        $matiere = new Matiere();
        $form = $this->createForm(MatiereChooseType::class, $matiere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->get('appbundle_matiere');
            $id_matiers = $data["nom"];
            $ids = array(9, 10);
            $query = $em->createQuery(
                'SELECT m.nom, c.nom FROM AppBundle:Matiere m
                JOIN m.category c 
                WHERE m.id IN (:id)'
            )->setParameter('id', $id_matiers);
            // var_dump($query->getResult());

            /*$participant->setNom($form["nom"]->getData());
            $participant->setPrenom($form["prenom"]->getData());
            $participant->setEmail($form["email"]->getData());
            $participant->setGender($form["gender"]->getData()[0]);

            $em = $this->getDoctrine()->getManager();
            $em->persist($participant);
            $em->flush();*/
//            $em = $this->getDoctrine()->getManager();
//            $em->persist($data);
//            $em->flush();

      //  $this->redirectToRoute("questionone");


        }


        return $this->render('default/questionthree.html.twig', array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Route("/q3", name ="questionthree")
     * add new Participants and category
     */
    public function createMatierAction(Request $request, Session $session)
    {


        $form = $this->createFormBuilder()
            ->add('nom', ChoiceType::class, array(
                'constraints' => New NotBlank(),
                'choices' => $this->matieres,
                'choice_label' => function($val, $key, $index) {

                    return $val;
                },
                'multiple' => false,
                'expanded' => true,
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $path = $this->get('kernel')->getRootDir() . '/Resources/metiers.json';
            
            $file = file_get_contents($path);
            $file = json_decode($file, true);
            $file = $file[0];
            $form_request = $request->request->get('form');


            $data = $form['nom']->getData();
//die($data);
            // cheked value from request
            //$my_request = array_rand($data, 1);


            // random matiere

            //$random_keys = array_rand($file, 1);


            // item key data final => nom metier
            $key = $file[$data];


            // get one metier
            $one_metier = array_rand($key, 1);

            // store data metier
            $session->set('oneMetier', $one_metier);

            //get image
            $image_metier = $key[$one_metier]['image'];

            // store data metier image
            $session->set('imageMetier', $image_metier);


            return $this->redirectToRoute('result');
        }

        return $this->render('default/questionthree.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/result", name ="result")
     */
    public function resultAction(Request $request, Session $session)
    {
        
        $metier = $session->get('oneMetier');
       
        $image = $session->get('imageMetier');
        return $this->render('default/result.html.twig', array(
            'metier' => $metier,
            'image' => $image
        ));
    }

    /**
     * @Route("/addcategory", name ="add_category")
     * add new Participants and category
     */
    public function createCategoryAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            // process upload file
            $file = $category->getImage();
            $id = $category->getId();
            $fileName = $file->getClientOriginalName();
            $file->move(
                $this->getParameter('image_category'),
                $fileName
            );
            $category->setImage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
        }

        return $this->render('default/addcategory.html.twig', array(
            'form' => $form->createView()
        ));
    }


}
