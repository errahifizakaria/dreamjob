<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;


    /**
     * Many Groups have Many Users.
     * @ORM\ManyToMany(targetEntity="Matiere", mappedBy="categories")
     */
    private $matieres;

    public function __construct() {
        $this->matieres = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add matiere
     *
     * @param \AppBundle\Entity\Matiere $matiere
     *
     * @return Category
     */
    public function setMatiere(\AppBundle\Entity\Matiere $matiere)
    {
        $this->matieres = $matiere;

        return $this;
    }

    /**
     * Remove matiere
     *
     * @param \AppBundle\Entity\Matiere $matiere
     */
    public function removeMatiere(\AppBundle\Entity\Matiere $matiere)
    {
        $this->matieres->removeElement($matiere);
    }

    /**
     * Get matieres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMatieres()
    {
        return $this->matieres;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Category
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
}
